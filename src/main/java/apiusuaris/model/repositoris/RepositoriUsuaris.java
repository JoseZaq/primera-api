package apiusuaris.model.repositoris;

import apiusuaris.model.entitats.Usuari;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoriUsuaris extends JpaRepository<Usuari,String> {
}
